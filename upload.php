<?php
  header('Content-Type: application/json; charset=utf-8');
  if($_POST["filename"]) {
    // *** uploadディレクトリチェック ***
    $current_dir = getcwd();
    $child_dir = "upload/";
    $dir = $current_dir."/".$child_dir;
    if(!file_exists($dir)){
      mkdir($dir, 0755);
    } else {
      // *** 既存ファイル削除 ***
      foreach (glob($dir.'*') as $file) {
        unlink($file);
      }
    }
    // *** ファイル情報取得 ****
    $exts = substr($_POST["filename"], strrpos($_POST["filename"], '.') + 1); // 拡張子取得
    $file = $_POST["file"];
    $file = preg_replace("/data:[^,]+,/i","",$file);
    $file = base64_decode($file);
    $image = imagecreatefromstring($file);
    imagesavealpha($image, TRUE);
    // *** ファイル変換処理 ****
    if($exts == "png") {
      imagepng($image, $dir.$_POST["filename"]);
    } else if ($exts == "jpeg" || $exts == "jpg") {
      imagejpeg($image, $dir.$_POST["filename"]);
    } else if ($exts == "gif") {
      imagegif($image, $dir.$_POST["filename"]);
    } else {
      echo json_encode("ERROR");
    }
    echo json_encode($_POST["filename"]);
  } else {
    echo json_encode("ERROR");
  }
?>
