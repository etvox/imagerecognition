ImageRecognition
====
* 画像解析ウェブツールのモックアップです。

## Description
* 画像ファイルドラッグ&ドロップ、ファイル選択
* アップロードしたファイルのURLの返却値（[ソース参照箇所](https://bitbucket.org/etvox/imagerecognition/src/601406a512f3a45e38fa82b81790fe2bdd1bb8f3/index.html?at=master&fileviewer=file-view-default#index.html-254)）。
* REST API の呼び出し（[ソース変更箇所①](https://bitbucket.org/etvox/imagerecognition/src/601406a512f3a45e38fa82b81790fe2bdd1bb8f3/index.html?at=master&fileviewer=file-view-default#index.html-269)、[ソース変更箇所②](https://bitbucket.org/etvox/imagerecognition/src/601406a512f3a45e38fa82b81790fe2bdd1bb8f3/index.html?at=master&fileviewer=file-view-default#index.html-440)）
* 解析結果のグラフ描画

## Demo
* http://app.etvox.com/imagerecognition/

## Requirement
* JSONの返却値は下記のようにしてください。
* 例）https://script.google.com/macros/s/AKfycbzvGip84Obx1jdhuV8rY3lVo3h1DnZ5-dtJLBRO2iAW81nkDvZa/exec

## Usage
* Apacheが稼働している環境

## Install

```
#!shell

git clone https://teijiro_shiotsuka@bitbucket.org/etvox/imagerecognition.git
```

## Licence
[MIT](http://app.etvox.com/imagerecognition/LICENSE.txt)

## Author
[TeijiroShiotsuka](https://bitbucket.org/teijiro_shiotsuka/)